# 使用官方 Node.js 基礎映像
FROM node:slim

# 創建並設定 app 目錄為工作目錄
WORKDIR /frontend
# WORKDIR /usr/src/app

# 複製 package.json 和 package-lock.json (如果可用)
COPY package*.json ./

# 安裝專案依賴
RUN npm install

# 複製所有文件到工作目錄
COPY . .

# 構建 Nuxt 應用
RUN npm run build

# 開放 3000 端口
EXPOSE 3000

# 運行 Nuxt 開發服務器
CMD ["node", ".output/server/index.mjs"]